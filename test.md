# 6.1.1 Отправить сообщение существующему клиенту в edna

Чтобы отправить сообщение существующему клиенту, необходимо указать
значение одного из следующих параметров: `clientId`,
`threadsClientId` или `threadId`.

``` bash
    ----
    Permissions:INTEGRATOR,OPERATOR,SUPERVISOR
    ----
```

  - Три типа ответов в порядке приоритетности

<!-- end list -->

  - 1\. По `threadsClientId`: Это внутренний идентификатор клиента,
    применимо и для авторизованных, и для неавторизованных
    клиентов.

  - 2\. По `clientId`:Это внешний идентификатор клиента. Чаще
    используется для авторизованных клиентов; не имеет
    привязки к конкретному треду.

  - 3\. По `threadId`: Это идентификатор треда, по которому вы можете
    идентифицировать клиента и отправить ему сообщение.

<table>
<caption>Поля тела запроса</caption>
<colgroup>
<col  />
<col  />
<col  />
</colgroup>
<thead>
<tr class="header">
<th>Параметр</th>
<th>Тип</th>
<th>Описание</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>clientId</code></p></td>
<td><p><code>String (необязательно)</code></p></td>
<td><p>Внешний идентификатор клиента</p></td>
</tr>
<tr class="even">
<td><p><code>threadsClientId</code></p></td>
<td><p><code>Long (необязательно)</code></p></td>
<td><p>Внутренний идентификатор клиента в edna</p></td>
</tr>
<tr class="odd">
<td><p><code>threadId</code></p></td>
<td><p><code>Long (необязательно)</code></p></td>
<td><p>Идентификатор треда, который требуется для определения клиента, которому вы хотите отправить сообщение</p></td>
</tr>
<tr class="even">
<td><p><code>text</code></p></td>
<td><p><code>String</code></p></td>
<td><p>Текст сообщения</p></td>
</tr>
<tr class="odd">
<td><p><code>attachments</code></p></td>
<td><p><code>List of objects (необязательно)</code></p></td>
<td><p>Список вложений сообщения</p></td>
</tr>
</tbody>
</table>

Пример объекта вложения:

> **Important**
> 
> Для того, чтобы файл верно отображался клиенту, название изображения
> должно содержать расширение файла, либо же должен быть указан тип
> файла (например, image/jpg).

``` json
  "attachments": [
    {
      "url": "http://...",
      "name": "test.jpg",
      "type": "image/jpeg",
      "size": 256

  ],
  ...
```

<table>
<colgroup>
<col  />
<col  />
<col  />
</colgroup>
<thead>
<tr class="header">
<th>Параметр</th>
<th>Тип</th>
<th>Описание</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>url</code></p></td>
<td><p>String</p></td>
<td><p>URL файла, строка до 4000 символов</p></td>
</tr>
<tr class="even">
<td><p><code>name</code></p></td>
<td><p>String</p></td>
<td><p>Название файла, строка до 1000 символов</p></td>
</tr>
<tr class="odd">
<td><p><code>type</code></p></td>
<td><p>String</p></td>
<td><p>MIME-тип файла, строка до 256 символов</p></td>
</tr>
<tr class="even">
<td><p><code>size</code></p></td>
<td><p>Integer</p></td>
<td><p>Размер файла в байтах</p></td>
</tr>
</tbody>
</table>

# Пример curl

``` bash
$ curl 'http://localhost:8080/api/v1/messages/outgoing' -i -X POST \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer <integrator_token>' \
    -d '{
  "text" : "message",
  "attachments" : [ ],
  "clientId" : "clientId"
'
```

# Пример HTTP запроса

Пример для `clientId`:

``` http
POST /api/v1/messages/outgoing HTTP/1.1
Content-Type: application/json
Authorization: Bearer <integrator_token>
Content-Length: 74
Host: localhost:8080

{
  "text" : "message",
  "attachments" : [ ],
  "clientId" : "clientId"

```

Пример для `threadsClientId`:

``` http
POST /api/v1/messages/outgoing HTTP/1.1
Content-Type: application/json
Authorization: Bearer <integrator_token>
Content-Length: 72
Host: localhost:8080

{
  "text" : "message",
  "attachments" : [ ],
  "threadsClientId" : 1

```

Пример для `threadId`:

``` http
POST /api/v1/messages/outgoing HTTP/1.1
Content-Type: application/json
Authorization: Bearer <integrator_token>
Content-Length: 42
Host: localhost:8080

{
  "text" : "message",
  "threadId" : 1

```

# Пример HTTP ответа

<table>
<caption>Поля тела ответа</caption>
<colgroup>
<col  />
<col  />
<col  />
</colgroup>
<thead>
<tr class="header">
<th>Параметр</th>
<th>Тип</th>
<th>Описание</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>id</code></p></td>
<td><p><code>Long</code></p></td>
<td><p>Уникальный идентификатор сообщения в edna</p></td>
</tr>
<tr class="even">
<td><p><code>threadId</code></p></td>
<td><p><code>Long</code></p></td>
<td><p>Уникальный идентификатора нового треда в edna для отправленного сообщения</p></td>
</tr>
<tr class="odd">
<td><p><code>clientId</code></p></td>
<td><p><code>Long</code></p></td>
<td><p>Уникальный идентификатор клиента, которому было отправлено сообщение</p></td>
</tr>
<tr class="even">
<td><p><code>clientExternalId</code></p></td>
<td><p><code>String</code></p></td>
<td><p>Уникальный внешний идентификатор клиента, которому было отправлено сообщение</p></td>
</tr>
<tr class="odd">
<td><p><code>agentId</code></p></td>
<td><p><code>Long</code></p></td>
<td><p>Уникальный идентификатор агента, отправившего сообщение</p></td>
</tr>
</tbody>
</table>

``` http
HTTP/1.1 200 OK
Vary: Origin
Vary: Access-Control-Request-Method
Vary: Access-Control-Request-Headers
Content-Type: application/json
Content-Length: 102

{
  "id" : 1,
  "threadId" : 2,
  "clientId" : 1,
  "clientExternalId" : "clientId",
  "agentId" : 3

```
